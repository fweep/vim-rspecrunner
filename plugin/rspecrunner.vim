" rspec-runner.vim
"
" Author: Jim Stewart <jim@fweep.com>
" Version: 0.0.1

"TODO move rspec formatter to lib dir, dynamically determine path based on
"this script
"TODO support test::unit?
"TODO config opt for rspec options
"TODO support failures in quickfix or location window based on config
"     file/line: default to location?  quickfix might still be best
"     all: default to quickfix
"TODO show quickfix window if file exists, in case of rspec warnings
"TODO figure out how to call functions in here globally
"     might just need to remove dashes and do rspecrunner#foo()

" Initialization {{{1

" if exists('g:loaded_rspec_runner') || &cp || v:version < 700
"   finish
" endif
" let g:loaded_rspec_runner = 1

function! rspecrunner#InComment()
  echo "InComment w00t!"
endfunction

" Key Mappings {{{1

if !exists('g:rspec_runner_suppress_keymap') || !g:rspec_runner_suppress_keymaps
  nnoremap <silent> <leader>r :call s:test_line()
  nnoremap <silent> <leader>R :call s:test_file()
  nnoremap <silent> <leader>A :call s:test_all()
endif

" Commands {{{1

command! -nargs=0 -buffer RSpecRunLine :call s:test_line()
command! -nargs=0 -buffer RSpecRunFile :call s:test_file()
command! -nargs=0 -buffer RSpecRunAll  :call s:test_all()

"TODO remove
command! -nargs=0 -buffer TestTest     :call s:TestTest()
function! s:TestTest()
  if s:file_empty('/tmp/pants')
    echo 'Empty!'
  else
    echo 'Not empty!'
  endif
endfunction

" Entry Points {{{1

function! s:test_line()
  call s:test('line')
endfunction

function! s:test_file()
  call s:test('file')
endfunction

function! s:test_all()
  call s:test('all')
endfunction

" Main {{{1

function! s:test(scope)
  call s:save_errorformat()
  let quickfix_filename = tempname()
  let stdout_filename = tempname()
  try
    call s:run_rspec(a:scope, quickfix_filename, stdout_filename)
    call s:show_quickfix(quickfix_filename)
    call s:show_rspec_command_errors(stdout_filename)
  finally
    call s:restore_errorformat()
    call s:remove_temp_files(quickfix_filename, stdout_filename)
  endtry
endfunction

" Quickfix/Display {{{1

function! s:show_quickfix(quickfix_filename)
  if filereadable(a:quickfix_filename) && !s:file_empty(a:quickfix_filename)
    "TODO use location list for line/file runs, controlled by option
    cfile errors
    "a:quickfix_filename
    "TODO configurable size for this and error window
    botright cwindow 5
    "TODO count failures
    if s:file_empty(a:quickfix_filename)
      echo 'RSpec completed with no failures.'
    else
      echo 'RSpec completed with failures.  See quickfix window.'
    endif
  endif
endfunction

function! s:show_rspec_command_errors(stdout_filename)
  if filereadable(a:stdout_filename) && !s:file_empty(a:stdout_filename)
    "TODO use location list for line/file runs, controlled by option
    " silent botright 5split a:stdout_filename
    botright 5new
    call append(0, readfile(a:stdout_filename))
    call cursor(1, 1)
    setlocal nomodifiable
    "TODO rename buffer
    "TODO delete buffer on close/unusedness?  or just use quickfix/location
    setlocal buftype=nofile
    call s:echo_warning('Error running RSpec!  ' . 'Check your RSpec configuration.')
  endif
endfunction

function! s:echo_warning(message)
  let old_cmdheight = &cmdheight
  try
    let &cmdheight = 2
    echohl WarningMsg | echo a:message | echohl None
  finally
    let &cmdheight = old_cmdheight
  endtry
endfunction

" RSpec Command Line {{{1

function! s:run_rspec(scope, quickfix_filename, stdout_filename)
  let rspec_command = s:get_rspec_command(a:scope, a:quickfix_filename, a:stdout_filename)
  echo 'Running: ' . rspec_command
  call system(rspec_command)
endfunction

function! s:get_rspec_command(scope, quickfix_filename, stdout_filename)
  let line_specifier = s:get_line_specifier(a:scope)
  let file_specifier = s:get_file_specifier(a:scope)
  "TODO put formatter in vim plugin directory and auto-determine path (some
  "other plugin knows how)
  let rspec_command = 'rspec ' . '-r ./spec/support/vim_quickfix_formatter ' . '-f VimQuickfixFormatter -o ' . a:quickfix_filename . ' ' . line_specifier . file_specifier . ' ' . '>' . a:stdout_filename . ' ' . '2>&1'
  return rspec_command
endfunction

" Utility {{{1

function! s:save_errorformat()
  let s:saved_errorformat = &l:errorformat
endfunction

function! s:restore_errorformat()
  let &l:errorformat = s:saved_errorformat
endfunction

function! s:get_line_specifier(scope)
  if a:scope == 'line'
    let line_specifier = '-l ' . line('.') . ' '
  else
    let line_specifier = ''
  endif
  return line_specifier
endfunction

function! s:get_file_specifier(scope)
  if a:scope == 'all'
    let file_specifier = 'spec'
  else
    let file_specifier = @%
  endif
  return file_specifier
endfunction

function! s:file_empty(filename)
  return getfsize(a:filename) == 0
endfunction

function! s:remove_temp_files(quickfix_filename, stdout_filename)
  call delete(a:quickfix_filename)
  call delete(a:stdout_filename)
endfunction

" modeline {{{1
" disabled_vim: ts=8 sw=4 sts=4 et foldenable foldmethod=marker foldcolumn=1
