require "rspec/core/formatters/base_formatter"

class VimQuickfixFormatter < RSpec::Core::Formatters::BaseFormatter

  def initialize(output)
    super(output)
  end

  def dump_failures
    return if failed_examples.empty?
    failed_examples.each do |example|
      exception = example.execution_result[:exception]
      message = exception.message
      error_line = exception.backtrace.find do |frame|
        frame =~ %r{\bspec/.*_spec\.rb:\d+:}
      end
      line_number = error_line.match(/:(\d+):/)[1]
      file = example.location.match(/^(.*):\d+$/)[1]
      output.puts "#{file}:#{line_number}: #{message}"
    end
  end

end
